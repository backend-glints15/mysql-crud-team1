const { query } = require('../models'); // Import connection from models

class Goods {
  // All Goods
  async getAllGoods(req, res, next) {
    try {
      // Find all goods data and order by id goods
      const data = await query('SELECT * FROM goods');

      // If data is empty array
      if (data.length === 0) {
        return res.status(404).json({ errors: ['Goods not found'] });
      }

      res.status(200).json({ data });
    } catch (error) {
      res.status(500).json({ errors: ['Internal Server Error'] });
    }
  }

  // Get Detail
  async getOneGoods(req, res, next) {
    try {
      // Find spesific goods by req.params.id
      const data = await query(
        'SELECT g.id, g.name, g.price, g.id_supplier FROM goods g JOIN suppliers s ON s.id= g.id_supplier WHERE g.id=?',
        [req.params.id]
      );

      // If data is empty array
      if (data.length === 0) {
        return res.status(404).json({ errors: ['Goods not found'] });
      }

      res.status(200).json({ data });
    } catch (error) {
      res.status(500).json({ errors: ['Internal Server Error'] });
    }
  }

  // Create goods
  async createGoods(req, res, next) {
    try {

      /* Async Await */

      // Insert Data
      const insertedData = await query(
        'INSERT INTO goods(name, price, id_supplier) VALUES (?, ?, ?)',
        [req.body.name, req.body.price, req.body.id_supplier]
      );

      // Find new Data
      const data = await query(
        'SELECT g.id, g.name, g.price, g.id_supplier FROM goods g JOIN suppliers s ON s.id= g.id_supplier WHERE g.id=?',
        [insertedData.insertId]
      );

      res.status(201).json({ data });
    } catch (error) {
      res.status(500).json({ errors: ['Internal Server Error'] });
    }
  }

  async updateGoods(req, res, next) {
    try {
        // Update goods data
        const updatedData = await query(
          "UPDATE goods g SET g.name=?, g.price=?, g.id_supplier=? WHERE id=?",
          [req.body.name, req.body.price, req.body.id_supplier,req.params.id]
        );
  
        if (updatedData.affectedRows === 0) {
          return res.status(404).json({
            errors: ["Goods not found"],
          });
        }
  
        //   Find updated Data
        const data = await query(
            'SELECT g.id, g.name, g.price, g.id_supplier FROM goods g JOIN suppliers s ON s.id= g.id_supplier WHERE g.id=?',
          [req.params.id]
        );
  
        res.status(200).json({ data });
      } catch (error) {
        console.log(error);
        res.status(500).json({ errors: ["Internal Server Error"] });
      }
  }

  async deleteGoods(req, res, next) {

    try {
        const deletedData = await query("DELETE FROM goods WHERE id=?", [
          req.params.id,
        ]);
  
        if (deletedData.affectedRows === 0) {
          return res.status(404).json({
            errors: ["Goods has been deleted or it's not exists"],
          });
        }
  
        res.status(200).json({ data: [] });
      } catch (error) {
        console.log(error);
        res.status(500).json({ errors: ["Internal Server Error"] });
      }
    }
  

}

module.exports = new Goods();