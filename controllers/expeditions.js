const { query } = require('../models');

class Expedition {
    async getAllExpedition(req, res, next) {
        try {
            const data = await query("SELECT * FROM expeditions");

            if (data.length === 0) {
                return res.status(404).json({ errors: 'Customer not found' });
            }

            res.status(200).json({ data })

        } catch (error) {
            res.status(500).json({ errors: 'Internal Server Error' });
        }
    }

    async getExpeditionById(req, res, next) {
        try {
            const data = await query(
                "SELECT e.id, e.name FROM expeditions e WHERE e.id=?",
                [req.params.id]
            );

            if (data.length === 0) {
                return res.status(404).json({ errors: 'Expedition not found' });
            }

            res.status(200).json({ data });

        } catch (error) {
            console.log(error);
            res.status(500).json({ errors: 'Internal Server Error' });
        }
    }

    async createExpedition(req, res, next) {
        try {
            const insertedData = await query(
                "INSERT INTO expeditions (name) VALUES (?)",
                [req.body.name]
            );

            const data = await query(
                "SELECT e.id, e.name FROM expeditions e WHERE e.id=?",
                [insertedData.insertId]
            );

            res.status(201).json({ data });
        } catch (error) {
            console.log(error);
            res.status(500).json({ errors: 'Internal Server Error' });
        }
    }

    async updateExpedition(req, res, next) {
        try {
            const updatedData = await query(
                "UPDATE expeditions e SET e.name=? WHERE id=?",
                [req.body.name, req.params.id]
            );

            if (updatedData.affectedRows === 0) {
                return res.status(404).json({ errors: 'Expeditioni not found' });
            }

            const data = await query(
                "SELECT * FROM expeditions WHERE id = ?",
                [req.params.id]
            );

            res.status(200).json({ data });

        } catch (error) {
            console.log(error);
            res.status(500).json({ errors: 'Internal Server Error' });
        }
    }

    async deleteExpedition(req, res, next) {
        try {
            const deletedData = await query("DELETE FROM expeditions WHERE id=?", [
                req.params.id
            ]);

            if (deletedData.affectedRows === 0) {
                return res.status(404).json({ errors: 'Expedition has been deleted or it\'s not exists' });
            }

            res.status(200).json({ data: [] });

        } catch (error) {
            console.log(error);
            res.status(500).json({ errors: 'Internal Server Error' });
        }
    }

}

module.exports = new Expedition();