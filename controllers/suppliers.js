const { query } = require("../models"); // Import connection from models

class Suppliers {
  // All suppliers
  async getAllSuppliers(req, res, next) {
    try {
      // Find all suppliers data and order by id suppliers
      const data = await query("SELECT id, name FROM suppliers;");
      console.log(data);
      // If data is empty array
      if (data.length === 0) {
        return res.status(404).json({ errors: ["suppliers not found"] });
      }

      res.status(200).json({ data });
    } catch (error) {
      res.status(500).json({ errors: ["Internal Server Error"] });
    }
  }

  // Get Detail
  async getOneSuppliers(req, res, next) {
    try {
      // Find spesific suppliers by req.params.id
      const data = await query("SELECT * FROM suppliers s WHERE id=?", [
        req.params.id,
      ]);

      // If data is empty array
      if (data.length === 0) {
        return res.status(404).json({ errors: ["Suppliers not found"] });
      }

      res.status(200).json({ data });
    } catch (error) {
      res.status(500).json({ errors: ["Internal Server Error"] });
    }
  }

  // Create Suppliers
  async createSuppliers(req, res, next) {
    try {
      // Insert Data
      const insertedData = await query(
        "INSERT INTO suppliers (name) VALUES (?)",
        [req.body.name]
      );

      // Find new Data
      const data = await query(
        "SELECT s.id, s.name FROM suppliers s WHERE s.id=?",
        [insertedData.insertId]
      );

      res.status(201).json({ data });
    } catch (error) {
      console.log(error);
      res.status(500).json({ errors: ["Internal Server Error"] });
    }
  }

  //update
  async updateSuppliers(req, res, next) {
    try {
      //update suppliers data
      const updateData = await query(
        "UPDATE suppliers s SET s.name=? WHERE s.id=?",
        [req.body.name, req.params.id]
      );

      const data = await query("SELECT s.name FROM suppliers s WHERE s.id=?", [
        req.params.id,
      ]);

      res.status(200).json({ data: data });
    } catch (error) {
      res.status(500).json({ errors: ["Internal Server Error"] });
    }
  }

  async deleteSuppliers(req, res, next) {
    try {
      const deleteData = await query("DELETE FROM suppliers WHERE id=?", [
        req.params.id,
      ]);

      if (deleteData.affectedRows === 0) {
        return res.status(404).json({
          errors: ["Suppliers has been deleted or it's not exists"],
        });
      }
      res.status(200).json({ data: [] });
    } catch (error) {
      console.log(error)
      res.status(500).json({ errors: ["Internal Server Error"] });
    }
  }
}

module.exports = new Suppliers();
