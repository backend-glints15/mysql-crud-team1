const express = require('express');
const transactions = require('./routes/transactions');
const goods = require('./routes/goods');
const suppliers = require('./routes/suppliers');
const customers = require('./routes/customers');
const expeditions = require('./routes/expeditions');

const app = express();

app.use(express.json());
app.use(express.urlencoded({ extended: true }));

app.use('/transaction', transactions);
app.use('/good', goods);
app.use('/supplier', suppliers);
app.use('/customer', customers);
app.use('/expedition', expeditions);

const port = process.env.port || 3000;
app.listen(port, () => console.log(`Server running on port ${port}...`));

