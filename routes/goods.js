const express = require('express');

// Import controller
const {
  createGoods,
  getAllGoods,
  getOneGoods,
  updateGoods,
  deleteGoods,
} = require('../controllers/goods');

const router = express.Router(); // Make router

router.get('/', getAllGoods);
router.post('/', createGoods);
router.get('/:id', getOneGoods);
router.put('/:id', updateGoods);
router.delete('/:id', deleteGoods);

module.exports = router;