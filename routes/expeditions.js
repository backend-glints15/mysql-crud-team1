const express = require('express');

const {
    getAllExpedition,
    getExpeditionById,
    createExpedition,
    updateExpedition,
    deleteExpedition
} = require('../controllers/expeditions');

const router = express.Router();

router.get('/', getAllExpedition);
router.get('/:id', getExpeditionById);
router.post('/', createExpedition);
router.put('/:id', updateExpedition);
router.delete('/:id', deleteExpedition);

module.exports = router;