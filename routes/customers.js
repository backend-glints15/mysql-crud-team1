const express = require("express");

// Import controller
const {
  createCustomer,
  getAllCustomer,
  getOneCustomer,
  updateCustomer,
  deleteCustomer,
} = require("../controllers/customers");

const router = express.Router(); // Make router

router.get("/", getAllCustomer);
router.post("/", createCustomer);
router.get("/:id", getOneCustomer);
router.put("/:id", updateCustomer);
router.delete("/:id", deleteCustomer);

module.exports = router;
