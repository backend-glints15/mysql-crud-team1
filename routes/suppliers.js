const express = require("express");

// Import controller
const {
  createSuppliers,
  getAllSuppliers,
  getOneSuppliers,
  updateSuppliers,
  deleteSuppliers,
} = require("../controllers/suppliers");

const router = express.Router(); // Make router

router.get("/", getAllSuppliers); // get all the data
router.post("/", createSuppliers); //create the data
router.get("/:id", getOneSuppliers); //get one the data
router.put("/:id", updateSuppliers); //update
router.delete("/:id", deleteSuppliers); //delete

module.exports = router;




