-- CREATE DATABASE with name sales
CREATE DATABASE sales_team1_pagi;

-- USE sales database
USE sales_team1_pagi;

-- CREATE suppliers TABLE
CREATE TABLE suppliers (
     id BIGINT UNSIGNED AUTO_INCREMENT PRIMARY KEY,
     name VARCHAR(255) NOT NULL
);

-- CREATE goods TABLE
CREATE TABLE goods (
     id BIGINT UNSIGNED AUTO_INCREMENT PRIMARY KEY,
     name VARCHAR(255) NOT NULL,
     price DECIMAL NOT NULL,
     id_supplier BIGINT UNSIGNED NOT NULL,
     FOREIGN KEY (id_supplier) REFERENCES suppliers(id)
);

-- CREATE customers TABLE
CREATE TABLE customers (
     id BIGINT UNSIGNED AUTO_INCREMENT PRIMARY KEY,
     name VARCHAR(255) NOT NULL
);

-- CREATE expeditions TABLE
CREATE TABLE expeditions (
	id bigint unsigned auto_increment not null primary key,
	name varchar(255) not null
);

-- CREATE transactions TABLE
CREATE TABLE transactions (
    id BIGINT UNSIGNED auto_increment NOT NULL PRIMARY KEY,
    id_good BIGINT UNSIGNED NOT NULL,
    id_customer BIGINT UNSIGNED NOT NULL,
    time DATETIME DEFAULT now() NOT NULL,
    quantity INT NOT NULL,
    total DECIMAL NOT NULL,
    id_expedition BIGINT UNSIGNED NOT NULL,
    FOREIGN KEY (id_good) REFERENCES goods(id),
    FOREIGN KEY (id_customer) REFERENCES customers(id),
    FOREIGN KEY (id_expedition) REFERENCES expeditions(id)
);

-- INSERT 
INSERT INTO customers(name) VALUES 
	("Wawan"),
	("Firman"),
	("Ilham");

INSERT INTO suppliers(name) VALUES 
	("Khay"),
	("Heru"),
	("Siti");

INSERT INTO goods(name, price, id_supplier) VALUES 
	("Pepsodent", 14500, 1),
	("Lifeboy", 24600, 2),
	("Clear", 44500, 3);

insert into expeditions (name) VALUES
	("SiCepat"),
	("JNE"),
	("J&T"),
	("AnterAja");

INSERT INTO transactions(id_good, id_customer, quantity, total, id_expedition) VALUES 
	(1, 1, 1, 14500, 1),
	(2, 2, 2, 49200, 2),
	(3, 3, 3, 133500, 3);

-- SELECT WITH JOIN
SELECT t.id, g.name as goodName, s.name as goodSupplier, g.price, c.name as customerName, e.name as expedition, t.time, t.quantity, t.total 
FROM transactions t
JOIN customers c ON t.id_customer=c.id
JOIN goods g ON g.id=t.id_good
JOIN suppliers s ON g.id_supplier=s.id
JOIN expeditions e ON e.id=t.id_expedition


-- SELECT
SELECT * FROM customers;

SELECT * FROM suppliers;

SELECT * FROM goods;

SELECT * FROM transactions;

select * from expeditions;
